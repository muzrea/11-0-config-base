package com.twuc.webApp.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@AutoConfigureMockMvc
class PropertyTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_properties() throws Exception {
        mockMvc.perform(get("/twuc/property")).andExpect(content().string("test"));
    }

    @Test
    void should_return_properties_use_environment() throws Exception {
        mockMvc.perform(get("/twuc/propertyUse")).andExpect(content().string("test"));
    }

    @Test
    void should_return_test_properties() throws Exception {
        mockMvc.perform(get("/twuc/id")).andExpect(content().string("id_config"));
    }

    @Test
    void should_return_properties_use_configuration_properties() throws Exception {
        mockMvc.perform(get("/twuc/propertyUseConfigure")).andExpect(content().string("test"));
    }
}
