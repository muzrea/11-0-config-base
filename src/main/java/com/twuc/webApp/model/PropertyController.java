package com.twuc.webApp.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PropertyController {
    @Value("${twuc.webapp.env}")
    private String env;

    @GetMapping("/twuc/property")
    public String getProperty() {
        return env;
    }

    @Value("${twuc.webapp.id}")
    private String id;

    @GetMapping("/twuc/id")
    public String getId() {
        return id;
    }

    @Autowired
    private Environment environment;

    @GetMapping("/twuc/propertyUse")
    public String getPropertyUseEnviroment() {
        return environment.getProperty("twuc.webapp.env");
    }


    @Autowired
    private TwucProperty twucProperty;

    @GetMapping("/twuc/propertyUseConfigure")
    public String getPropertyUseConfigureProperty() {
        return twucProperty.getEnv();
    }

}
